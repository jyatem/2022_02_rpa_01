Sample
Text & Image PDF
It was November. Although it was not yet late,
the sky was dark when I turned into Laun-
dress Passage. I closed the door and put the
shop key in its usual place behind Bailey's
Advanced Principles of Geometry. Poor
Bailey. No one has wanted his fat gray book
for thirty years. Sometimes I wonder what he
makes of his role as guardian of the bookshop
keys. I dontt suppose itis the destiny he had in
mind for the masterwork that he spent ti.vo
decades writing.
A [etter. For me. That was something of an
event. The crisp-cornered envelope, puffed up
its thickly folded contents, was addressed
in a hand that must have given the postman a
certain amount of trouble. Although the Style
of the writing was old-fashioned, its heavily embellished capitals and curly flour-
ishes, my first impression was that it had been
written by a chi[d. The letters seemed
untrained. Their uneven strokes either faded
into nothing or were heavily etched into the
paper. There was no sense of flow in the
letters that out my name. Each had
been undertaken separately M A R G A R
E T L E A as a new and daunting enter-
prise. But I knew no chi[dren. That is when I
thought, It is the hand of an invalid.
It gave me a queer feeling. Yesterday or the
day before, ivvhile I had been going about my
l)llsitlcss. lictlv a 11 d D riva te. sorne
Nunc congue venenatis malesuada.
In nec tempor massa. Sed rutrum magna vel nunc
lacinia, facilisis tincidunt mi vulputate.
Lorem ipsum dolor sit amet, consectetur
adipiscing elit. Morbi congue, libero ac
consequat tellus velit bibendum elit,
eget fringilla odio nisl vitae libero, Nam
feugiat viverra ipsum. In ac magna ac diam
dapibus gravida. Vestibulum at ex facilisis,
aliquamjusto et, dignissim enim. Aenean
pharetraa risus ac pharetra tempus, purus nisi
finibus risus, vel aliquetjusto lorem ut lacus.
Donec luctus orci ac placerat ultrices. Donec
nunc erat, accumsan ac risus quis, faucibus
pretium magna. Donec nisl ipsum, feugiat
quis arcu eta rhoncus dapibus lacus, Donec
malcsuada ct est ct convallis. Aliquam id diam
nisi. Vestibulum non eget dolor pellen- tesque tincidunt. Aenean odio odio, semper
non finibus vel, dictum vitae sem. Sed quis
felis tristique, suscipit massa sed, fermentum
nisl,
Aenean risus ligula, vestibulum nec semper
ac, vestibulum et nisl. Integer et accumsan est.
Mauris sollicitudin erat vel turpis ornare
vehicula. Morbi id egestas quarn. Duis in
justo ultricies, feugiat eros eget, ornare justo.
Donec tincidunt eleifend efficitur. Ut male-
suada ultrices accumsan. Morbi ac mi ac est
vehicula condimentum ac sit amet dui.